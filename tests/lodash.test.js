const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 

import { sorting, compare, average } from '../src/utils/helper';


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

// avg
test('Should return average of an array', () => {
	const avg = average([-3, 0,3]);
	expect(avg).to.equal(0);
})

test('Should return average of an array', () => {
	const avg = average([-0,  5]);
	expect(avg).to.equal(2.5);
})

test('Should return average of an array', () => {
	const avg = average([1, "",3]);
	expect(avg).to.equal(2);
})

test('Should return average of an array', () => {
	const avg = average([1, "dddddddd",3]);
	expect(avg).to.equal(2);
})

test('Should return average of an array', () => {
	const avg = average([]);
	expect(avg).to.equal(0);
})

// sort
test('Should sort the array in ascending order', () => {
	const arr = [1, 11, 111, 2, 22, 222];
	const arrSorted = [1, 2, 11, 22, 111, 222];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})

test('Should sort the array in ascending order', () => {
	const arr = [1, -1, 11, -11];
	const arrSorted = [-11, -1, 1, 11];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})

test('Should sort the array in ascending order', () => {
	const arr = [10, 12, "", 22];
	const arrSorted = ["", 10, 12, 22];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})

test('Should sort the array in ascending order', () => {
	const arr = [10, 12, "abc", 22];
	const arrSorted = ["abc", 10, 12, 22];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})

test('Should sort the array in ascending order', () => {
	const arr = [];
	const arrSorted = [];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})
