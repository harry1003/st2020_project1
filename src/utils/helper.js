
let cp = (a, b) => {
    if (parseFloat(a).toString() == "NaN"){
        return -1;
    }
    if (parseFloat(b).toString() == "NaN"){
        return 1;
    }
    if(parseFloat(a)  > parseFloat(b))
        return 1;
    else if(parseFloat(a)  < parseFloat(b))
        return -1;
    return 0;
}

let sorting = (array) => {
    array.sort(cp);
    return array;
}

let compare = (a, b) => {
    return cp(a["PM2.5"], b["PM2.5"]);
}

let average = (nums) => {
    if(nums.length == 0) return 0;
    var num = 0;
    var total = 0;
    for(var i=0; i<nums.length; i++){
        if (parseFloat(nums[i]).toString() !== "NaN") { 
            num = num + 1;
            total = total + nums[i];
        } 
    }
    var avg = parseFloat(total) / num;
	return parseFloat(avg.toFixed(2));
}


module.exports = {
    sorting,
    compare,
    average
}